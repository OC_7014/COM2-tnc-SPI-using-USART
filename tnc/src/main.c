/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * This is a bare minimum user application template.
 *
 * For documentation of the board, go \ref group_common_boards "here" 
 * for a link
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# Minimal main function that starts with a call to board_init()
 * -# Basic usage of on-board LED and button
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: 
 *  visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */


/* 
 * \brief The code configures USART module in SPI master mode 
 *        and write/read data to a register of cc1120
 *
 */

#include "modules.h"
#include <drivers/pio/pio.h>		
#define DEBUG
#ifdef DEBUG
#define led(x) ioport_set_pin_level(LED_0_PIN, !x)
#endif

#define USART_SERIAL                 USART1
#define USART_SERIAL_ID              ID_USART1  
#define USART_SERIAL_BAUDRATE        115200
#define USART_SERIAL_CHAR_LENGTH     US_MR_CHRL_8_BIT
#define USART_SERIAL_PARITY          US_MR_PAR_NO
#define USART_SERIAL_STOP_BIT        US_MR_NBSTOP_1_BIT
#define PINS_USART1_PIO      PIOA
#define PINS_USART1_ID       ID_USART1
#define PINS_USART1_TYPE     PIO_PERIPH_A
#define PINS_USART1_ATTR     PIO_DEFAULT
#define PINS_USART1_MASK     (PIO_PA21A_RXD1| PIO_PA22A_TXD1| \
                              PIO_PA23A_SCK1| PIO_PA24A_RTS1 | PIO_PA7) 


void keep_blinking_modified() {
	int count = 0;
	for (int a = 0; a < 4; a++) {       // Twice on-off
		led(count % 2);
		delay_ms(1000);
		count = count ^ 1;
	}
}

int main (void) {
	sysclk_init();
	board_init();
	pio_configure(PINS_USART1_PIO, PINS_USART1_TYPE, 
                  PINS_USART1_MASK, PINS_USART1_ATTR);
	pmc_enable_periph_clk(ID_USART1);

    const usart_spi_opt_t usart_spi_settings = {
        USART_SERIAL_BAUDRATE,
        USART_SERIAL_CHAR_LENGTH,
        SPI_MODE_0,
        US_MR_CHMODE_NORMAL
    };

	sysclk_enable_peripheral_clock(USART_SERIAL_ID);
	Usart *p_usart = USART_SERIAL;
    usart_init_spi_master(p_usart, &usart_spi_settings, 
                          sysclk_get_peripheral_hz());
    usart_spi_force_chip_select(p_usart);
    usart_enable_tx(p_usart);
	usart_enable_rx(p_usart);
    NVIC_EnableIRQ(USART1_IRQn);
    uint32_t i = 0;
    for (i = 0; i < 5; i++) {               // 5 times sent data
        // request to write data in 0x01 register
        usart_write(p_usart, 0x01);
        while (!usart_is_tx_ready(p_usart));
    
        // data to be written in the given address
        usart_write(p_usart, i);            
        while (!usart_is_tx_ready(p_usart));
    
        // request to read data from cc
        usart_write(p_usart, 0x81);
        while (!usart_is_tx_ready(p_usart));
    
        // clock cycle to read data
        usart_write(p_usart, 0x00);
        while (!usart_is_tx_ready(p_usart));

        // check the data that is received to the controller
        uint32_t c;
        usart_read(p_usart, &c);

        // if received same as transmitted, blink LED
        if (c == i) {
            keep_blinking_modified();
        }
    }
    return 0;

}

// usart_read
